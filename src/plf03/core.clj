(ns plf03.core)
;-----------------------------Primer commit--------------------
(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (inc x))
        h (fn [x] (inc x))
        z (comp f g h)]
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (inc x))
        z (comp g f)]
    (z 8)))

(defn función-comp-3
  []
  (let [f (fn [x] (+ 3 x))
        g (fn [x] (+ 3 x))
        z (comp g f)]
    (z 8)))

(defn función-comp-4
  []
  (let [f (fn [x] (+ 3 x))
        g (fn [x] (+ 3 x))
        h (fn [xs] (vector xs))
        z (comp h g f)]
    (z -5)))

(defn función-comp-5
  []
  (let [f (fn [xs] (first xs))
        g (fn [x] (* 10 x))
        h (fn [xs] (vector xs))
        z (comp h g f)]
    (z [10 90 6])))

(defn función-comp-6
  []
  (let [f (fn [xs] (second xs))
        g (fn [x] (* 10 x))
        h (fn [x] (int? x))
        z (comp h g f)]
    (z [5.2 9.4 6])))

(defn función-comp-7
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (not x))
        h (fn [xs] (list xs))
        z (comp h g f)]
    (z 5.2)))

(defn función-comp-8
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (not x))
        h (fn [xs] (list xs))
        z (comp g f h)]
    (z 5.2)))

(defn función-comp-9
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (not x))
        h (fn [xs] (list xs))
        z (comp  h h g g f h)]
    (z 5.2)))

(defn función-comp-10
  []
  (let [f (fn [x] (* x x))
        g (fn [xs] (set xs))
        h (fn [xs] (list xs))
        z (comp g h f )]
    (z 5)))

(defn función-comp-11
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (map dec xs))
        z (comp g f)]
    (z #{1 9 6 5 0 3})))

(defn función-comp-12
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (map dec xs))
        z (comp f g)]
    (z #{1 9 6 5 0 3})))

(defn función-comp-13
  []
  (let [f (fn [xs] (into [] (remove neg? xs)))
        g (fn [xs] (reverse xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z '(5 9 -4 0 3 8))))

(defn función-comp-14
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        g (fn [xs] (reverse xs))
        h (fn [xs] (map dec xs))
        z (comp h h h f g)]
    (z [9 8 7 6 4 -5])))

(defn función-comp-15
  []
  (let [f (fn [xs] (into [] (remove neg? xs)))
        g (fn [xs] (last xs))
        h (fn [xs] (map inc xs))
        z (comp  g f h)]
    (z [1 2 -5 4 -9])))

(defn función-comp-16
  []
  (let [f (fn [xs] (into '() (remove neg? xs)))
        g (fn [xs] (first xs))
        h (fn [xs] (map inc xs))
        z (comp  g f h )]
    (z [9 5 1 5 7 0 0 0 2 0])))

(defn función-comp-17
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (not x))
        h (fn [xs] (list xs))
        z (comp  h g g f )]
    (z 10.3)))

(defn función-comp-18
  []
  (let [f (fn [x] (not x))
        g (fn [xs] (filterv false? xs))
        h (fn [xs] (list xs))
        z (comp  h f g)]
    (z '(true false))))

(defn función-comp-19
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (not x))
        h (fn [xs] (vector xs))
        z (comp  h g f g )]
    (z 0.245)))

(defn función-comp-20
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (map dec xs))
        z (comp g f)]
    (z [7 90 6 -5])))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

;---------------------------------------Fin primer commit--------------------------
;---------------------------------------Incia segundo commit------------------------

(defn función-complement-1
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 10)))

(defn función-complement-2
  []
  (let [f (fn [x] (int? x))
        z (complement f)]
    (z 10)))

(defn función-complement-3
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 10.2)))

(defn función-complement-4
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z "Aloo")))

(defn función-complement-5
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z "A")))

(defn función-complement-6
  []
  (let [f (fn [xs] (vector? xs))
        z (complement f)]
    (z '(\a \l \o))))

(defn función-complement-7
  []
  (let [f (fn [xs] (list? xs))
        z (complement f)]
    (z '(5 9 0))))

(defn función-complement-8
  []
  (let [f (fn [xs] (set? xs))
        z (complement f)]
    (z #{5 10 9 8 7 6})))

(defn función-complement-9
  []
  (let [f (fn [xs] (list? xs))
        z (complement f)]
    (z '(5 9 0))))

(defn función-complement-10
  []
  (let [f (fn [x] (empty? x))
        z (complement f)]
    (z '())))

(defn función-complement-11
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 7)))

(defn función-complement-12
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 10))) 

(defn función-complement-13
  []
  (let [f (fn [xs c] (contains? c xs))
        z (complement f)]
    (z \a ["n" "a"])))

(defn función-complement-14
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z -2.5)))

(defn función-complement-15
  []
  (let [f (fn [xs] (decimal?(first xs)))
        z (complement f)]
    (z [1.2 8 9.5 5.1])))

(defn función-complement-16
  []
  (let [f (fn [xs] (pos-int? (first (range xs))))
        z (complement f)]
    (z 5)))

(defn función-complement-17
  []
  (let [f (fn [x] false? x)
        z (complement f)]
    (z true)))

(defn función-complement-18
  []
  (let [f (fn [xs] float? (last xs))
        z (complement f)]
    (z [\a "Aloo" 5 4 9 0.6])))

(defn función-complement-19
  []
  (let [f (fn [xs] odd? (+ (first xs) (second xs)))
        z (complement f)]
    (z [5 4 9 8])))

(defn función-complement-20
  []
  (let [f (fn [xs ys] double? (+ (apply max xs) (apply max ys)))
        z (complement f)]
     (z [ 5 4 9 0.6] [10.9 7 9 5 ])))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)
;-----------------------------------------------------Fin segundo commit-------------------
;-----------------------------------------------------Inicio tercer commit-----------------

(defn función-constantly-1
  []
  (let [xs [\a \b \c]
        z (constantly xs)]
    (z [1 2 3 4 5])))

(defn función-constantly-2
  []
  (let [x true
        z (constantly x)]
    (z '(4 9 0))))

(defn función-constantly-3
  []
  (let [x false
        z (constantly x)]
    (z )))

(defn función-constantly-4
  []
  (let [xs {9 9 7 9}
        z (constantly xs)]
    (z '(\a \l \o))))

(defn función-constantly-5
  []
  (let [xs [[true false] '("A" "S")]
        z (constantly xs)]
    (z 5)))

(defn función-constantly-6
  []
  (let [xs [\z]
        z (constantly xs)]
    (z true)))

(defn función-constantly-7
  []
  (let [xs [{:a '("Hanson")}]
        z (constantly xs)]
    (z {:a "MMMBop"})))

(defn función-constantly-8
  []
  (let [xs [{:a 5 :b 7} #{8 9}]
        z (constantly xs)]
    (z {9 9})))

(defn función-constantly-9
  []
  (let [x "String cadena"
        z (constantly x)]
    (z {6 5 4 6})))

(defn función-constantly-10
  []
  (let [x 1.9M
        z (constantly x)]
    (z 6.4)))

(defn función-constantly-11
  []
  (let [f ((fn [xs] (sorted? xs)) [1 4 3 4 5])
        z (constantly f)]
    (z [1 2 3 4 5])))

(defn función-constantly-12
  []
  (let [f ((fn [xs] (sorted? (sorted-set xs))) [1 4 3 4 5])
        z (constantly f)]
    (z [1 2 3 4 5])))

(defn función-constantly-13
  []
  (let [f ((fn [xs] (sorted? (shuffle xs))) [1 2 3 4 5])
        z (constantly f)]
    (z [1 2 3 4 5])))

(defn función-constantly-14
  []
  (let [f ((fn [xs] (int? (first xs))) [2.0 1 5 6])
        z (constantly f)]
    (z [8 9])))

(defn función-constantly-15
  []
  (let [f ((fn [xs] (int? (second xs)))[[1.1 2.2] [2.1 2.1] [2 3]])
        z (constantly f)]
    (z [8 9])))

(defn función-constantly-16
  []
  (let [f ((fn [x] (even? (first (range x)))) 5)
        z (constantly f)]
    (z 8)))

(defn función-constantly-17
  []
  (let [f ((fn [x] (dec x)) 5)
        z (constantly f)]
    (z 7)))

(defn función-constantly-18
  []
  (let [f ((fn [x] (inc (inc x))) 5)
        z (constantly f)]
    (z 7)))

(defn función-constantly-19
  []
  (let [f ((fn [x y] mod y x) 8 32)
        z (constantly f)]
    (z 35 5)))

(defn función-constantly-20
  []
  (let [x ['(7 9 0) [#{1 4 3 2 5} #{1 4 3 2 5}]]
        z (constantly x)]
    (z #{1 4 2 8})))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)
;-----------------------------------------------------------------Fin tercer commit--------------------------------------------
;-----------------------------------------------------------------Inicia cuarto commit-----------------------------------------

(defn función-every-pred-1
  []
  (let [f (fn [x] int? x)
        z (every-pred f)]
    (z 7)))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (every-pred number? odd?) xs)
        z (every-pred f)]
    (z 3 9 11)))

(defn función-every-pred-3
  []
  (let [f (fn [xs] (every-pred pos? ratio?) xs)
        g (fn [xs] (filter xs))
        z (every-pred f g )]
    (z [0 2/3 -2/3 1/4 -1/10 5 3/3])))

(defn función-every-pred-4
  []
  (let [f (fn [xs] (every-pred pos? double?) xs)
        z (every-pred f)]
    (z [1 1 -1])))

(defn función-every-pred-5
  []
  (let [f (fn [xs] (peek xs))
        g (fn [xs] (pop xs))
        z (every-pred f g)]
    (z [0 1 2 3 4 5 6 7 8 9])))

(defn función-every-pred-6
  []
  (let [f (fn [xs] list? xs)
        g (fn [xs] (zero? (first xs)))
        z (every-pred f g)]
    (z '(0 9 0 7 8 6 5))))

(defn función-every-pred-7
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (shuffle xs))
        z (every-pred f g)]
    (z [9 5 1 5 7 0 0 0 2 0])))

(defn función-every-pred-8
  []
  (let [f (fn [x] boolean? x)
        g (fn [x] true? x )
        z (every-pred f g)]
    (z true)))

(defn función-every-pred-9
  []
  (let [f (fn [xs] (string? xs))
        g (fn [xs] (map? xs))
        z (every-pred f g)]
    (z {:a 9  :c 8  :e 7})))

(defn función-every-pred-10
  []
  (let [f (fn [x] nil? x)
        g (fn [x] zero? x)
        z (every-pred f g)]
    (z 0)))

(defn función-every-pred-11
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (zipmap [] xs))
        z (every-pred f g)]
    (z [:a :e :i :o])))

(defn función-every-pred-12
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (>= 8 (first xs)))
        z (every-pred f g)]
    (z '(5 6 0 9))))

(defn función-every-pred-13
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [xs] (not (char? xs)))
        z (every-pred f g)]
    (z [true \a])))

(defn función-every-pred-14
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (>= 8 (last xs)))
        z (every-pred f g)]
    (z '(5 6 0 9))))

(defn función-every-pred-15
  []
  (let [f (fn [x] (ident? x))
        g (fn [xs] (indexed? xs))
        h (fn [x] (keyword? x))
        z (every-pred f g h)]
    (z :a)))

(defn función-every-pred-16
  []
  (let [f (fn [xs] (coll? xs))
        z (every-pred f f f)]
    (z '(:17161210 "Oscar" :17161158 "Sergio"))))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (seqable? xs))
        g (fn [xs] (seq? xs))
        z (every-pred f g)]
    (z [10 9 8 9])))

(defn función-every-pred-18
  []
  (let [f (fn [x] double? x)
        g (fn [x] (<= 0.3 (* 30 x)))
        h (fn [x] (>= 0.5 x))
        z (every-pred f g h)]
    (z 0.000004689858)))

(defn función-every-pred-19
  []
  (let [f (fn [xs] (sorted? xs))
        g (fn [xs] (set? xs))
        z (every-pred f g)]
    (z {5 6 7 8})))

(defn función-every-pred-20
  []
  (let [f (fn [x] (> x 70))
        z (every-pred f )]
    (z 98)))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)
;--------------------------------------------------------------Fin cuarto commit-------------------------------------------------
;--------------------------------------------------------------Inicio quinto commit----------------------------------------------

(defn función-fnil-1
  []
  (let [f (fn [x] (inc x))
        z (fnil f 0)]
    (z nil))) 

(defn función-fnil-2
  []
  (let [f (fn [x] (> x 5))
        z (fnil f 9 )]
    (z nil )))

(defn función-fnil-3
  []
  (let [f (fn [xs] (distinct xs))
        z (fnil f [:a \b :c \d :e \a])]
       (z nil )))

(defn función-fnil-4
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
        z (fnil f 4)]
    (z nil 5)))

(defn función-fnil-5
  []
  (let [f (fn [x y] (if (> x y) (* x y) (/ x y)))
        z (fnil f 9)]
    (z nil 9)))

(defn función-fnil-6
  []
  (let [f (fn [xs] (vector xs))
        z (fnil f [6 9  0 9 5] )]
    (z nil)))

(defn función-fnil-7
  []
  (let [f (fn [xs] (int? xs))
        z (fnil f 780)]
    (z nil )))

(defn función-fnil-8
  []
  (let [f (fn [x] (float x))
        z (fnil f 2.5)]
    (z nil)))

(defn función-fnil-9
  []
  (let [f (fn [xs] (peek xs))
        z (fnil f ["A" "L" "O" "O"])]
    (z nil)))

(defn función-fnil-10
  []
  (let [f (fn [xs] (rest xs))
        z (fnil f [\A \L \O] )]
    (z nil)))

(defn función-fnil-11
  []
  (let [f (fn [xs] (shuffle xs))
        z (fnil f [[88 7] '(8 66 0)] )]
    (z nil)))

(defn función-fnil-12
  []
  (let [f (fn [xs x] (contains? xs x))
        z (fnil f {:17161060 "Jennifer" :17161210 "Alexis"} :17161060)]
    (z nil nil)))

(defn función-fnil-13
  []
  (let [f (fn [x] (keyword x))
        z (fnil f :b )]
    (z nil)))

(defn función-fnil-14
  []
  (let [f (fn [xs] (map? xs))
        z (fnil f {:17161158 "Lopez" :17161210 "Ramirez"})]
    (z nil)))

(defn función-fnil-15
  []
  (let [f (fn [x] (double? x))
        z (fnil f 12.4)]
    (z nil))) 

(defn función-fnil-16
  []
  (let [f (fn [xs] (keys xs))
        z (fnil f {:17161170 :17161158 :17161508 :17161060})]
    (z nil)))

(defn función-fnil-17
  []
  (let [f (fn [x y] (map char (range x y)))
        z (fnil f 48)]
       (z nil 58))) 

(defn función-fnil-18
  []
  (let [f (fn [x y] (inc (+ x y)))
        z (fnil f 15 7)]
    (z nil nil)))

(defn función-fnil-19
  []
  (let [f (fn [x y] (dec (* x y)))
        z (fnil f 7 7)]
    (z nil 7)))

(defn función-fnil-20
  []
  (let [f (fn [xs] (string? xs))
        z (fnil f ["O" "S" "C" "A" "R"])]
    (z nil)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)
;-----------------------------------------------------------------Fin quinto commit-------------------------------------
;-----------------------------------------------------------------Inicia Sexto commit-----------------------------------

(defn función-juxt-1
  []
  (let [f (fn [x] (string? (first x)))
        g (fn [x] false? x)
        z (juxt f g)]
    (z ["O" "S" "C" "A" "R"])))

(defn función-juxt-2
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (* 3 (first xs)))
        z (juxt f g)]
    (z [7 1 0 6 5])))

(defn función-juxt-3
  []
  (let [f (fn [xs] (count xs) )
        g (fn [xs] repeat xs)
        z (juxt f g)]
    (z ["S" "O" "L"])))

(defn función-juxt-4
  []
  (let [f (fn [xs] sort xs)
        g (fn [xs] (set xs))
        z (juxt f g)]
    (z [7 1 0 6 5]))) 

(defn función-juxt-5
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "Alooo Me oyen!")))

(defn función-juxt-6
  []
  (let [f (fn [xs] (* (first xs) (second xs) ))
        g (fn [xs] (list xs))
        z (juxt f g)]
    (z '(7 9 0 75 9)))) 

(defn función-juxt-7
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (count (str xs)))
        z (juxt f g)]
    (z '(7 9 0 75 9) )))

(defn función-juxt-8
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (count (str xs)))
        z (juxt f g)]
    (z [7 90 4 6 7])))

(defn función-juxt-9
  []
  (let [f (fn [x] (first x))
        g (fn [x] (str x))
        z (juxt f g)]
    (z {2 4 1 4})))

(defn función-juxt-10
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (associative? xs))
        z (juxt f g)]
    (z {:discord "Alexis7595" :slack "si" })))

(defn función-juxt-11
  []
  (let [f (fn [x] (int x))
        g (fn [x] (boolean? x))
        z (juxt f g)]
    (z 7595)))

(defn función-juxt-12
  []
  (let [f (fn [x] double? x)
        g (fn [x] decimal? x)
        z (juxt f g)]
    (z 100.1)))

(defn función-juxt-13
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (pos? x))
        z (juxt f g)]
    (z -2.0)))

(defn función-juxt-14
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (pos? x))
        h (fn [x] (str x))
        z (juxt f g h)]
    (z -2.0)))

(defn función-juxt-15
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (pos? x))
        h (fn [x] (str x))
        z (juxt f g h)]
    (z 5.9)))

(defn función-juxt-16
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (pos? x))
        h (fn [x] (str x))
        i(fn [x] list x)
        z (juxt f g h i)]
    (z 4)))

(defn función-juxt-17
  []
  (let [f (fn [xs] (second xs))
        g (fn [xs] (last xs))
        h (fn [xs] (first xs))
        z (juxt f g h)]
    (z {9 9 4 6 3 0 1 4})))

(defn función-juxt-18
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
        g (fn [x y] (if (> x y) (* x y) (/ x y)))
        z (juxt f g)]
    (z  5 7)))

(defn función-juxt-19
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (last xs))
        h (fn [xs] (repeat xs))
        z (juxt f g h)]
    (z #{9 6 4 5 1})))

(defn función-juxt-20
  []
  (let [f (fn [xs] (list xs))
        g (fn [xs] (repeat (list xs)))
        h (fn [xs] (* 5 (last xs)))
        z (juxt f g h)]
    (z #{9 6 4 5 1 0 10})))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

;----------------------------------------------Fin sexto commit---------------------
;----------------------------------------------Inicio septimo commit----------------

(defn función-partial-1
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f 5)]
    (z 9 8)))

(defn función-partial-2
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f 4 9)]
    (z 8)))

(defn función-partial-3
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f )]
    (z 3 9 8)))

(defn función-partial-4
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f 5 4 8)]
    (z )))

(defn función-partial-5
  []
  (let [f (fn [x] (cond
                    (= 0 x) 1
                    (= 1 x) 0))
        z (partial f 1)]
    (z)))
(defn función-partial-6
  []
  (let [f (fn [x y z] (if (< x y z) (* x z ) (/ x y)))
        z (partial f )]
    (z 7 9 0)))

(defn función-partial-7
  []
  (let [f (fn [xs ys] (str xs ys))
        z (partial f [7 9 0 4 6 3])]
    (z [1 9 0 7 5 6 4])))

(defn función-partial-8
  []
  (let [f (fn [xs ys] (str xs ys))
        z (partial f )]
    (z '(true false)[1 9 0 7 5 6 4])))

(defn función-partial-9
  []
  (let [f (fn [xs] (str (sort xs)))
        z (partial f)]
    (z [1 9 0 7 5 6 4])))

(defn función-partial-10
  []
  (let [f (fn [xs] (str (sort xs)))
        z (partial f)]
    (z [9 8 7 0 -5 -4 6])))

;------------------------------------------------------Fin septimo commit------------------------------------------
;------------------------------------------------------Inicio octavo commit------------------------------------

(defn función-partial-11
  []
  (let [f (fn [xs ys zs] (list xs ys zs))
        z (partial f [\a \b \c] {:b \a :c \d})]
    (z ["Aloo" "Catalina"])))

(defn función-partial-12
  []
  (let [f (fn [xs ys zs] (list xs ys zs))
        z (partial f  {:b \a :c \d})]
    (z [1 9 0 7 5 6 4] [true false])))

(defn función-partial-13
  []
  (let [f (fn [xs ys zs] (list xs (sort-by :b ys) zs))
        z (partial f [true false] {:b \a :c \d})]
    (z [:a :b :c :d])))

(defn función-partial-14
  []
  (let [f (fn [xs ys] (sort-by :b (vector xs ys)))
        z (partial f {:b \a :c \d})]
    (z [:b :c :d :e])))

(defn función-partial-15
  []
  (let [f (fn [x y z w] (max x y z w))
        z (partial f 5 9 0)]
    (z -1)))

(defn función-partial-16
  []
  (let [f (fn [x y z w] (min x y z w))
        z (partial f 5 -2 -9)]
    (z -1)))

(defn función-partial-17
  []
  (let [f (fn [xs ys] (mapv xs ys))
        z (partial f dec)]
    (z [0 8 90 7 9])))

(defn función-partial-18
  []
  (let [f (fn [xs ys] (list (str xs) (str ys)))
        z (partial f #{1 9 5 0 7 6 3 4})]
    (z {1 2 3 4})))

(defn función-partial-19
  []
  (let [f (fn [x ys] (group-by x ys))
        z (partial f set)]
    (z ["Life" "is" "a" "Highway"])))

(defn función-partial-20
  []
  (let [f (fn [xs] (set xs))
        z (partial f)]
    (z {1 2 3 4})))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

;-------------------------------------------------------Fin octavo commit-----------------------------------------
;------------------------------------------------------Inicia Noveno commit----------------------------------------

(defn función-some-fn-1
  []
  (let [f (fn [x] (even? x))
        z (some-fn f)]
    (z 2)))
(defn función-some-fn-2
  []
  (let [f (fn [x] (odd? x))
        z (some-fn f)]
    (z 7)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (char? x))
        h (fn [x] (int? x))
        z (some-fn f g h)]
    (z \q)))

(defn función-some-fn-4
  []
  (let [f (fn [x] flatten x)
        g (fn [x] first x)
        z (some-fn f g )]
    (z 5)))

(defn función-some-fn-5
  []
  (let [f (fn [xs] flatten xs)
        g (fn [xs] first xs)
        z (some-fn f g)]
    (z [9 7 9 6 0])))

(defn función-some-fn-6
  []
  (let [f (fn [xs] (double? (first xs)) )
        g (fn [xs] second xs)
        z (some-fn f g)]
    (z [7 9 0 6 5 4 3])))

(defn función-some-fn-7
  [] 
  (let [f (fn [xs] (frequencies xs))
        g (fn [xs] (first xs))
        h (fn [xs] (int xs))
        z (some-fn f g h)]
    (z [2 2 2 3 3 true "Frio" "Frio" "Frio" "5:30"])))

(defn función-some-fn-8
  []
  (let [f (fn [xs] (frequencies xs))
        g (fn [xs] (second xs))
        h (fn [xs] (int xs))
        z (some-fn f g h)]
    (z [1 1 1 1 false false 4 9 9 true true])))

(defn función-some-fn-9
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z 47)))

(defn función-some-fn-10
  []
  (let [f (fn [xs] (pop xs))
        g (fn [xs] (vector? xs))
        h (fn [xs] (dec xs))
        z (some-fn f g h)]
    (z '(9 9 9 1 0 8))))

;------------------------------------------------------Fin Noveno commit----------------------------------------
;------------------------------------------------------Inicia Decimo Commit-------------------------------------

(defn función-some-fn-11
  []
  (let [f (fn [x] (range x))
        g (fn [x] (distinct? x))
        z (some-fn g f )]
    (z 4)))

(defn función-some-fn-12
  []
  (let [f (fn [xs] (odd? (first xs)))
        g (fn [xs] (map? xs))
        h (fn [xs] (set? xs))
        z (some-fn h f g )]
    (z '(7 9 0 2 3 4 6 6))))

(defn función-some-fn-13
  []
  (let [f (fn [xs] (boolean xs))
        g (fn [xs] (set xs))
        h (fn [xs] (list? xs))
        z (some-fn g h f)]
    (z [1 8 4 2 3])))

(defn función-some-fn-14
  []
  (let [f (fn [xs] (sort xs))
        g (fn [xs] (double? xs))
        z (some-fn f g )]
    (z [5 9 1 9 35 -0.52 0.3 54])))

(defn función-some-fn-15
  []
  (let [f (fn [x] (count x))
        z (some-fn f)]
    (z "Aloooo")))
  
(defn función-some-fn-16
  []
  (let [f (fn [xs] (drop-last xs))
        g (fn [xs] (drop xs))
        z (some-fn f g )]
    (z {:17161060 \M :17161158 \F :17161210 \M})))

  (defn función-some-fn-17
    []
    (let [f (fn [x] (list? x))
          z (some-fn f)]
      (z {:Git "yes"})))

(defn función-some-fn-18
  []
  (let [f (fn [xs] (conj xs))
        z (some-fn f)]
    (z [4 8 12 16 32 64])))

(defn función-some-fn-19
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (false? xs))
        z (some-fn g f )]
    (z [true false true])))

(defn función-some-fn-20
  []
  (let [f (fn [x] (string? x))
        z (some-fn f)]
    (z "Es covid, TriggerFish")))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)
;--------------------------------------------Fin decimo Commit----------------------------------------------------
